<?php

namespace Drupal\widen_media\Widen;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;

/**
 * Widen Asset Class.
 */
final class WidenAsset implements CacheableDependencyInterface {

  use CacheableDependencyTrait;

  /**
   * Date related to the asset.
   *
   * @var array
   */
  protected array $data = [];

  /**
   * Constructor for creating a new asset.
   *
   * @param array $data
   *   The data from the media asset.
   *
   * @internal
   */
  private function __construct(array $data) {
    $this->data = $data;
    $this->data['created_date'] = new \DateTime($data['created_date']);
    $this->data['last_update_date'] = new \DateTime($data['last_update_date']);
    $this->data['file_upload_date'] = new \DateTime($data['file_upload_date']);
    if (!is_null($data['deleted_date'])) {
      $this->data['deleted_date'] = new \DateTime($data['deleted_date']);
    }
  }

  /**
   * Create the Widen Asset.
   *
   * @return self
   *   Return the new widen asset item.
   */
  public static function create(array $data) {
    return new self($data);
  }

  /**
   * Return the ID of the asset.
   *
   * @return string|null
   *   The asset id.
   */
  public function getId() {
    return $this->data['id'];
  }

  /**
   * Return the External ID of the asset.
   *
   * @return string|null
   *   The external id.
   */
  public function getExternalId() {
    return $this->data['external_id'];
  }

  /**
   * Return the Filename for the asset.
   *
   * @return string|null
   *   Return the
   */
  public function getFileName() {
    return $this->data['filename'];
  }

  /**
   * Return the Created Date.
   *
   * @return \DateTime
   *   The Created Date and Time.
   */
  public function getCreatedDate() {
    return $this->data['created_date'];
  }

  /**
   * Return the Last Updated Date.
   *
   * @return \DateTime
   *   The Last Updated Date and Time.
   */
  public function getLastUpdateDate() {
    return $this->data['last_update_date'];
  }

  /**
   * Return the File Uploaded Date.
   *
   * @return \DateTime
   *   The File Upload Date.
   */
  public function getFileUploadDate() {
    return $this->data['file_upload_date'];
  }

  /**
   * Return the Deleted Date.
   *
   * @return \DateTime|null
   *   The Deleted Date and Time.
   */
  public function getDeletedDate() {
    return $this->data['deleted_date'];
  }

  /**
   * Return The Released and Not Expired data.
   *
   * @return bool
   *   The Released and Not Expired status.
   */
  public function getReleasedAndNotExpired() {
    return $this->data['released_and_not_expired'];
  }

  /**
   * Get the asset properties.
   *
   * @return array
   *   The asset properties.
   */
  public function getAssetProperties() {
    return $this->data['asset_properties'];
  }

  /**
   * Get the file properties.
   *
   * @return array
   *   The file properties.
   */
  public function getFileProperties() {
    return $this->data['file_properties'];
  }

  /**
   * Return the metadata properties.
   *
   * @return array
   *   The metadata properties.
   */
  public function getMetadata() {
    return $this->data['metadata'];
  }

  /**
   * Return information for the security.
   *
   * @return array
   *   The security information.
   */
  public function getSecurity() {
    return $this->data['security'];
  }

  /**
   * Return the list of the thumbnails.
   *
   * @return array
   *   Return the thumbnails.
   */
  public function getThumbnails() {
    return $this->data['thumbnails'];
  }

  /**
   * Return the embed data.
   *
   * @return array
   *   List of embeds.
   */
  public function getEmbeds() {
    return $this->data['embeds'];
  }

  /**
   * Get the list of links for the asset.
   *
   * @return string[]
   *   The list of links.
   */
  public function getLinks() {
    return $this->data['_links'];
  }

  /**
   * Return the Download URL.
   *
   * @return string
   *   The download url.
   */
  public function getDownloadUrl() {
    $links = $this->getLinks();
    return $links['download'];
  }

  /**
   * Return field metadata information.
   *
   * @param string $field
   *   The field to return the information for.
   *
   * @return array|null
   *   The data for the field selected.
   */
  public function getMetdataInfo($field) {
    $metadata = $this->getMetadata();
    if (isset($metadata['fields'][$field])) {
      return $metadata['fields'][$field];
    }
    return [];
  }

  /**
   * Return Details about the Asset.
   *
   * @return array|null
   *   The data associated with the properties.
   */
  public function getProperties() {
    $type = $this->getFileType();
    if ($type == 'image') {
      return $this->getFileProperties()['image_properties'];
    }
    if ($type == 'video') {
      return $this->getFileProperties()['video_properties'];
    }
    return [];
  }

  /**
   * Return the File Type for the media.
   *
   * @return false|string
   *   Get the file type.
   */
  public function getFileType() {
    $type = $this->getFileProperties()['format_type'];

    switch ($type) {
      case 'office':
      case 'pdf':
        return 'document';

      default:
        return $type;
    }
  }

  /**
   * Return the URL for the provided size.
   *
   * @param string $size
   *   The size of the thumbnail to return.
   *
   * @return string|false
   *   The URL associated with the thumbnail.
   */
  public function getThumbNailUrl($size) {
    $thumbnails = $this->getThumbnails();

    return $thumbnails[$size]['url'] ?? FALSE;
  }

  /**
   * Return the embed type for the following asset.
   *
   * @param string $type
   *   The type of embed to search for.
   * @param string $output
   *   The type of output to return, examples url, html, share.
   *
   * @return string
   *   The output for the requested embed type and output.
   */
  public function getEmbed($type, $output) {
    $embeds = $this->getEmbeds();
    if (isset($embeds[$type]) && isset($embeds[$type][$output])) {
      return $embeds[$type][$output];
    }
    return '';
  }

}
