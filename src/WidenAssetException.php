<?php

namespace Drupal\widen_media;

/**
 * Exception thrown if a Widen Asset cannot be fetched or parsed.
 *
 * @internal
 *   This is an internal part of the Widen Media system and should only be used
 *   by widen-related code in Drupal core.
 */
class WidenAssetException extends \Exception {

}
