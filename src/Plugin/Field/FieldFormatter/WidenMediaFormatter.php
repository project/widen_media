<?php

namespace Drupal\widen_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\widen_media\Service\Widen as WidenAPI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field Formatter for Widen Media.
 *
 * @FieldFormatter(
 *   id = "widen_media",
 *   label = @Translation("Widen Media Embed Content"),
 *   field_types = {
 *     "link",
 *     "string"
 *   },
 * )
 */
class WidenMediaFormatter extends FormatterBase {

  /**
   * Widen API Service.
   *
   * @var \Drupal\widen_media\Service\Widen
   */
  protected $widenApi;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->widenApi = $container->get('widen_media.api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $defaults = [
      'display' => 'original',
      'output' => 'html',
    ];
    return [
      'embed_display' => 'html',
      'document_embed' => $defaults,
      'image_embed' => $defaults,
      'video_embed' => $defaults,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $source_url = $item->value;
      $asset = $this->widenApi->getAsset($source_url);
      $fileType = $asset->getFileType();
      $settings = $this->getSetting($fileType . '_embed');

      if ($fileType === FALSE || $settings === NULL) {
        $element[$delta] = NULL;
      }
      else {
        $url = $asset->getEmbed($settings['display'], 'url');
        if ($settings['output'] === 'html') {
          $element[$delta] = [
            '#type' => 'inline_template',
            '#template' => '<div style="position:relative;width:100%;height:0;padding-bottom:56.25%;"><iframe title="{{ title }}" src="{{ url }}" width="100%" height="" webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder="0" allowtransparency="true" scrolling="no" style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>',
            '#context' => [
              'title' => $this->t('Video for &quot;@video&quot;', ['@video' => $asset->getFileName()]),
              'url' => $url,
            ],
          ];
        }
        else {
          $element[$delta] = [
            '#type' => 'link',
            '#title' => $asset->getFileName(),
            '#url' => $url,
          ];
        }
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $types = ['document', 'image', 'video'];

    foreach ($types as $type) {
      $key = $type . '_embed';

      $form[$key] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@type Embeds', ['@type' => ucwords($type)]),
        '#description' => $this->t('Embeds to Use when Saving Data'),
        '#tree' => TRUE,
      ];

      $form[$key]['display'] = [
        '#type' => 'select',
        '#options' => WidenAPI::getEmbedTypes($type),
        '#title' => $this->t('@type Embed Type', ['@type' => ucwords($type)]),
        '#default_value' => $this->getSetting($key)['display'],
        '#required' => TRUE,
      ];

      $form[$key]['output'] = [
        '#type' => 'select',
        '#options' => $this->getEmbedOptions(),
        '#title' => $this->t('@type Embed', ['@type' => ucwords($type)]),
        '#default_value' => $this->getSetting($key)['output'],
        '#required' => TRUE,
      ];
    }

    return $form;
  }

  /**
   * Return the list of embed options.
   *
   * @return array
   *   The keys associated with the embed options.
   */
  private function getEmbedOptions() {
    return [
      'url' => $this->t('Url to Asset'),
      'html' => $this->t('HTML to Embed Asset'),
      'share' => $this->t('URL to Share Asset'),
    ];
  }

}
