<?php

namespace Drupal\widen_media\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\widencollective\WidencollectiveAuthService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Widen Collective Media Auth Controller.
 */
class WidencollectiveSiteAuthController extends ControllerBase {

  /**
   * Request Stack service.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $requestStack;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Constructor for WidencollectiveSiteAuthController.
   *
   * @param \Drupal\Core\Http\RequestStack $requestStack
   *   The request stack factory.
   * @param \Drupal\Core\State\State $state
   *   The state factory.
   */
  public function __construct(RequestStack $requestStack, State $state) {
    $this->requestStack = $requestStack;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('state')
    );
  }

  /**
   * Callback used for authenticating against Widen Service.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return the authentication response.
   */
  public function authenticate() {
    // Get the code returned by the Widen Collective API endpoint, if available.
    $code = $this->requestStack->getCurrentRequest()->query->get('code');

    if (isset($code)) {
      // Save returned code to the current user profile.
      $this->handleAuthentication($code);
      $this->messenger()->addStatus($this->t('Account authorized to Widen Collective.'));
    }
    else {
      $this->messenger()->addError($this->t('Authorization Denied. Widen Collective did not provide an auth code.'));
    }
    return $this->redirect('widencollective.admin');
  }

  /**
   * Checks whether given account is valid and updates account information.
   *
   * @param string $auth_code
   *   The authorization code provided during user creation.
   */
  private function handleAuthentication($auth_code) {
    $response = WidencollectiveAuthService::authenticate($auth_code);

    // If account is valid is and a token code has been provide, update the
    // account of the current user and set widen credentials saving the
    // widen_username and widen_token values.
    if (isset($response->username) && isset($response->access_token)) {
      $account = [
        'widen_username' => $response->username,
        'widen_token' => $response->access_token,
      ];

      $this->state->set('widencollective_auth', $account);

      // Redirect back to user edit form.
      $redirect = Url::fromRoute('widencollective.admin')->toString();
      $response = new RedirectResponse($redirect);
      $response->send();

      return;
    }
    // Else, display a message to the user.
    else {
      $error_msg = $this->t('Authorization Failure');
      if (isset($response->error)) {
        $error_msg .= ' ' . $this->t('[@error: @description]', [
          '@error' => $response->error,
          '@description' => $response->description,
        ]);
      }

      $this->messenger()->addError($error_msg);
    }

  }

}
