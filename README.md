# Widen Media

INTRODUCTION
------------

Widen develop software solutions for marketers who need to connect their
visual content – like graphics, logos, photos, videos, presentations and
more – for greater visibility and brand consistency.

This module allows your Drupal projects to import media into Drupal

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
   for further information.

CONFIGURATION
-------------

 * Configure Widen Collective Configuration

   - Configure Widen Collective Module

     Before any steps can go futher the Widen Collective module will need to be
     enabled and installed.

 * Configure Full Widen Access:

   - Widen Collective Authorization

     Enter your username and password to enable your access to Widen Collective.
     Configuration will be enabled site wide and allows individuals to import
     media using the Asset URL from Widen.

FAQ
---

Q: Are the usernames/passwords being stored in the database?

A: This Drupal module does not store anything. The Authentication token is
   stored within the State service.

MAINTAINERS
-----------

Current maintainers:
* Kanopi - https://www.drupal.org/kanopi
